<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="borkedlabs-passives">
<packages>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="CBA-SILK-LOGO">
<circle x="0" y="0" radius="0.254" width="0.127" layer="21"/>
<circle x="-0.762" y="0.762" radius="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="1.016" x2="0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="-0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.254" x2="1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.254" x2="0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.508" x2="1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="0.508" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="-1.016" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.508" x2="-1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="MK-LOGO-SILK">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-1.016" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="1.016" x2="-0.1905" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="-1.016" x2="0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.1905" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.1905" y2="1.016" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
</symbols>
<devicesets>
<deviceset name="CBA-LOGO">
<gates>
</gates>
<devices>
<device name="" package="CBA-SILK-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MK-LOGO">
<gates>
</gates>
<devices>
<device name="" package="MK-LOGO-SILK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3_STANDOFF">
<pad name="P$1" x="0" y="0" drill="4.4" diameter="7" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3.6"/>
<vertex x="0.6" y="3.6"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3.6"/>
<vertex x="-0.6" y="-3.6"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.6" y="-0.6"/>
<vertex x="-3.6" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.6" y="0.6"/>
<vertex x="3.6" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.95269375" y="2.13136875"/>
<vertex x="-2.104165625" y="2.979896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.99705625" y="-2.12131875"/>
<vertex x="2.148528125" y="-2.969846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.104165625" y="-2.969846875"/>
<vertex x="-2.95269375" y="-2.12131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.148528125" y="2.979896875"/>
<vertex x="2.99705625" y="2.13136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.125" layer="51"/>
</package>
<package name="QFN16-3X3-TI-RTE">
<description>3x3 mm</description>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.025" width="0.1016" layer="21"/>
<wire x1="1.025" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.025" width="0.1016" layer="21"/>
<wire x1="-1.025" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.025" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-1.025" x2="1.5" y2="-1.5" width="0.1016" layer="21"/>
<circle x="-1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0" y="0" radius="0.15" width="0.01" layer="49"/>
<circle x="0.5" y="0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="0.5" y="-0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-0.5" y="-0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-0.5" y="0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<smd name="TH" x="0" y="0" dx="1.7" dy="1.7" layer="1" stop="no" cream="no"/>
<smd name="1" x="-1.475" y="0.75" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="2" x="-1.475" y="0.25" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="3" x="-1.475" y="-0.25" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="4" x="-1.475" y="-0.75" dx="0.85" dy="0.3" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="5" x="-0.75" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-0.25" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="7" x="0.25" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="8" x="0.75" y="-1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="9" x="1.475" y="-0.75" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="10" x="1.475" y="-0.25" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="11" x="1.475" y="0.25" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="12" x="1.475" y="0.75" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="13" x="0.75" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="14" x="0.25" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="15" x="-0.25" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="16" x="-0.75" y="1.475" dx="0.85" dy="0.3" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<text x="-2" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.875" y1="-0.875" x2="0.875" y2="0.875" layer="29"/>
<rectangle x1="-0.8" y1="0.1" x2="-0.1" y2="0.8" layer="31" rot="R90"/>
<rectangle x1="-1.9" y1="0.55" x2="-1.2" y2="0.95" layer="29"/>
<rectangle x1="-1.85" y1="0.625" x2="-1.2" y2="0.875" layer="31"/>
<rectangle x1="-1.9" y1="0.05" x2="-1.2" y2="0.45" layer="29"/>
<rectangle x1="-1.85" y1="0.125" x2="-1.2" y2="0.375" layer="31"/>
<rectangle x1="-1.85" y1="-0.375" x2="-1.2" y2="-0.125" layer="31"/>
<rectangle x1="-1.85" y1="-0.875" x2="-1.2" y2="-0.625" layer="31"/>
<rectangle x1="-1.075" y1="-1.65" x2="-0.425" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.575" y1="-1.65" x2="0.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.075" y1="-1.65" x2="0.575" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="0.425" y1="-1.65" x2="1.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="1.2" y1="-0.875" x2="1.85" y2="-0.625" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="-0.375" x2="1.85" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.125" x2="1.85" y2="0.375" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.625" x2="1.85" y2="0.875" layer="31" rot="R180"/>
<rectangle x1="0.425" y1="1.4" x2="1.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.075" y1="1.4" x2="0.575" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.575" y1="1.4" x2="0.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-1.075" y1="1.4" x2="-0.425" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.8" y1="-0.8" x2="-0.1" y2="-0.1" layer="31" rot="R180"/>
<rectangle x1="0.1" y1="-0.8" x2="0.8" y2="-0.1" layer="31" rot="R270"/>
<rectangle x1="0.1" y1="0.1" x2="0.8" y2="0.8" layer="31"/>
<rectangle x1="-1.9" y1="-0.45" x2="-1.2" y2="-0.05" layer="29"/>
<rectangle x1="-1.9" y1="-0.95" x2="-1.2" y2="-0.55" layer="29"/>
<rectangle x1="-1.1" y1="-1.75" x2="-0.4" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.6" y1="-1.75" x2="0.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.1" y1="-1.75" x2="0.6" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="0.4" y1="-1.75" x2="1.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="1.2" y1="-0.95" x2="1.9" y2="-0.55" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="-0.45" x2="1.9" y2="-0.05" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.05" x2="1.9" y2="0.45" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.55" x2="1.9" y2="0.95" layer="29" rot="R180"/>
<rectangle x1="0.4" y1="1.35" x2="1.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.1" y1="1.35" x2="0.6" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.6" y1="1.35" x2="0.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-1.1" y1="1.35" x2="-0.4" y2="1.75" layer="29" rot="R270"/>
<wire x1="-1.5" y1="1.02" x2="-1.03" y2="1.5" width="0.127" layer="21"/>
<circle x="-2.06" y="1.24" radius="0.15" width="0.127" layer="21"/>
</package>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="21"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="21"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="21"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="21"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
</package>
<package name="NRS5020T4R7MMGJ">
<smd name="1" x="0" y="1.65" dx="4.9" dy="1.5" layer="1"/>
<smd name="2" x="0" y="-1.65" dx="4.9" dy="1.5" layer="1"/>
<text x="-3" y="2.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.1" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="PWRPAD_3-25MM-SKINNY">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.35" thermals="no"/>
</package>
<package name="PWRPAD_M25_STANDOFF">
<pad name="P$1" x="0" y="0" drill="3.7" diameter="6" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="1.9"/>
<vertex x="-0.4" y="1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-1.9"/>
<vertex x="0.4" y="-1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-1.9" y="0.4"/>
<vertex x="-1.9" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.5"/>
<vertex x="3" y="-0.7"/>
<vertex x="1.9" y="-0.5"/>
<vertex x="1.9" y="0.3"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-0.99203125" y="1.584921875"/>
<vertex x="-1.55771875" y="1.0192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.49705625" y="-1.72131875"/>
<vertex x="1.648528125" y="-2.569846875"/>
<vertex x="1.03639375" y="-1.574871875"/>
<vertex x="1.60208125" y="-1.0091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.669846875"/>
<vertex x="-2.55269375" y="-1.82131875"/>
<vertex x="-1.55771875" y="-1.1091875"/>
<vertex x="-0.99203125" y="-1.674871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.60208125" y="1.0192375"/>
<vertex x="1.03639375" y="1.584921875"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="BUCK-TS30011-12-13">
<pin name="BST" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="VSW" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="PG" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="VCC" x="-15.24" y="10.16" length="middle"/>
<pin name="EN" x="-15.24" y="2.54" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="PGND" x="-15.24" y="-7.62" length="middle"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<pin name="FB" x="15.24" y="-5.08" length="middle" rot="R180"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-STANDOFF" package="PWRPAD_M3_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-SKINNY" package="PWRPAD_3-25MM-SKINNY">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5_STANDOFF" package="PWRPAD_M25_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUCK-TS30011-12-13" prefix="U">
<gates>
<gate name="G$1" symbol="BUCK-TS30011-12-13" x="0" y="0"/>
</gates>
<devices>
<device name="QFN" package="QFN16-3X3-TI-RTE">
<connects>
<connect gate="G$1" pin="BST" pad="10"/>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="4 TH"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="PGND" pad="14 15"/>
<connect gate="G$1" pin="VCC" pad="2 3 11"/>
<connect gate="G$1" pin="VSW" pad="1 12 13 16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="NRS5020T4R7MMGJ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microcontrollers">
<packages>
<package name="TQFP64_14X14MM-013THIN">
<description>64-Lead TQFP Plastic Thin Quad Flatpack - 14x14x1mm Body</description>
<wire x1="7" y1="7" x2="-7" y2="7" width="0.127" layer="51"/>
<wire x1="-7" y1="7" x2="-7" y2="-7" width="0.127" layer="51"/>
<wire x1="-7" y1="-7" x2="7" y2="-7" width="0.127" layer="51"/>
<wire x1="7" y1="-7" x2="7" y2="7" width="0.127" layer="51"/>
<wire x1="-7.1" y1="6.4" x2="-7.1" y2="7.1" width="0.127" layer="21"/>
<wire x1="-7.1" y1="7.1" x2="-6.4" y2="7.1" width="0.127" layer="21"/>
<wire x1="6.4" y1="7.1" x2="7.1" y2="7.1" width="0.127" layer="21"/>
<wire x1="7.1" y1="7.1" x2="7.1" y2="6.4" width="0.127" layer="21"/>
<wire x1="7.1" y1="-6.4" x2="7.1" y2="-7.1" width="0.127" layer="21"/>
<wire x1="7.1" y1="-7.1" x2="6.4" y2="-7.1" width="0.127" layer="21"/>
<wire x1="-7" y1="-6.4" x2="-6.4" y2="-7" width="0.127" layer="21"/>
<smd name="56" x="-7.6" y="0.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="57" x="-7.6" y="-0.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="58" x="-7.6" y="-1.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="55" x="-7.6" y="1.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="54" x="-7.6" y="2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="59" x="-7.6" y="-2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="60" x="-7.6" y="-2.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="53" x="-7.6" y="2.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="52" x="-7.6" y="3.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="61" x="-7.6" y="-3.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="62" x="-7.6" y="-4.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="51" x="-7.6" y="4.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="50" x="-7.6" y="5.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="63" x="-7.6" y="-5.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="49" x="-7.6" y="6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="64" x="-7.6" y="-6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="8" x="-0.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="9" x="0.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="10" x="1.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="7" x="-1.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="6" x="-2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="11" x="2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="12" x="2.8" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="5" x="-2.8" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="4" x="-3.6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="13" x="3.6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="14" x="4.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="3" x="-4.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="2" x="-5.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="15" x="5.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="1" x="-6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="16" x="6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="24" x="7.6" y="-0.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="25" x="7.6" y="0.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="26" x="7.6" y="1.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="23" x="7.6" y="-1.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="22" x="7.6" y="-2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="27" x="7.6" y="2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="28" x="7.6" y="2.8" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="21" x="7.6" y="-2.8" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="20" x="7.6" y="-3.6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="29" x="7.6" y="3.6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="30" x="7.6" y="4.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="19" x="7.6" y="-4.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="18" x="7.6" y="-5.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="31" x="7.6" y="5.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="17" x="7.6" y="-6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="32" x="7.6" y="6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="40" x="0.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="41" x="-0.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="42" x="-1.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="39" x="1.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="38" x="2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="43" x="-2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="44" x="-2.8" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="37" x="2.8" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="36" x="3.6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="45" x="-3.6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="46" x="-4.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="35" x="4.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="34" x="5.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="47" x="-5.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="33" x="6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="48" x="-6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<text x="-8.89" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="9.525" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<circle x="-7.62" y="-7.62" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="QFN-64-9X9MM">
<description>&lt;h3&gt;64-pin QFN 9x9mm, 0.5mm pitch&lt;/h3&gt;
&lt;p&gt;Package used by ATmega128RFA1&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.atmel.com/Images/Atmel-8266-MCU_Wireless-ATmega128RFA1_Datasheet.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-4.492" y1="-4.5" x2="4.508" y2="-4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="-4.5" x2="4.508" y2="4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="4.5" x2="-4.492" y2="4.5" width="0.09" layer="51"/>
<wire x1="-4.492" y1="4.5" x2="-4.492" y2="-4.5" width="0.09" layer="51"/>
<wire x1="-4.6" y1="4.6" x2="-4.6" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="4.6" x2="-4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="4.1" width="0.2032" layer="21"/>
<circle x="-4.842" y="4.85" radius="0.2" width="0" layer="21"/>
<circle x="-3.442" y="3.45" radius="0.2" width="0.09" layer="51"/>
<smd name="26" x="0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="25" x="0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="24" x="-0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="27" x="1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="28" x="1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="22" x="-1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="21" x="-1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="6" x="-4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="5" x="-4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="4" x="-4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="7" x="-4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="8" x="-4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="3" x="-4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="2" x="-4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="9" x="-4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="10" x="-4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="1" x="-4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="16" x="-4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="15" x="-4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="14" x="-4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="17" x="-3.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="18" x="-3.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="13" x="-4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="12" x="-4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="19" x="-2.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="20" x="-2.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="11" x="-4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="29" x="2.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="30" x="2.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="31" x="3.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="32" x="3.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="33" x="4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="34" x="4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="35" x="4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="36" x="4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="37" x="4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="38" x="4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="39" x="4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="40" x="4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="41" x="4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="42" x="4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="43" x="4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="44" x="4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="45" x="4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="46" x="4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="48" x="4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="49" x="3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="50" x="3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="51" x="2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="52" x="2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="53" x="1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="54" x="1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="55" x="0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="56" x="0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="57" x="-0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="58" x="-0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="59" x="-1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="60" x="-1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="61" x="-2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="62" x="-2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="63" x="-3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="64" x="-3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.6" y1="-4.6" x2="4.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.1" y2="-4.6" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="0" dx="7.6" dy="7.6" layer="1" cream="no"/>
<polygon width="0.127" layer="31">
<vertex x="2.13" y="2.13"/>
<vertex x="2.13" y="3.27"/>
<vertex x="3.27" y="3.27"/>
<vertex x="3.27" y="2.13"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.27" y="2.13"/>
<vertex x="-3.27" y="3.27"/>
<vertex x="-2.13" y="3.27"/>
<vertex x="-2.13" y="2.13"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.27" y="-3.27"/>
<vertex x="-3.27" y="-2.13"/>
<vertex x="-2.13" y="-2.13"/>
<vertex x="-2.13" y="-3.27"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.13" y="-3.27"/>
<vertex x="2.13" y="-2.13"/>
<vertex x="3.27" y="-2.13"/>
<vertex x="3.27" y="-3.27"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.57" y="-0.57"/>
<vertex x="-0.57" y="0.57"/>
<vertex x="0.57" y="0.57"/>
<vertex x="0.57" y="-0.57"/>
</polygon>
</package>
<package name="PDI_2X3_SMD">
<description>&lt;h3&gt;Surface Mount - 2x3&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:2.54mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.81" y1="-2.5" x2="-3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.5" x2="3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.5" x2="3.81" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.5" x2="-3.81" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-0.3" y1="2.55" x2="0.3" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="2.55" x2="-2.24" y2="3.35" layer="51"/>
<rectangle x1="2.24" y1="2.55" x2="2.84" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="-3.35" x2="-2.24" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-0.3" y1="-3.35" x2="0.3" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="2.24" y1="-3.35" x2="2.84" y2="-2.55" layer="51" rot="R180"/>
<smd name="1" x="-2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="2" x="-2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="3" x="0" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="4" x="0" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="5" x="2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="6" x="2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<text x="-1.397" y="0.381" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.778" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-1.27" y1="-4.38" x2="1.27" y2="-4.38" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ATXMEGA_A3U">
<pin name="RESET/PDI_CLK" x="-25.4" y="66.04" length="middle"/>
<pin name="PDI_DATA" x="-25.4" y="60.96" length="middle"/>
<pin name="AVCC" x="-25.4" y="50.8" length="middle"/>
<pin name="VCC" x="-25.4" y="45.72" length="middle"/>
<pin name="GND" x="-25.4" y="38.1" length="middle"/>
<pin name="PR0/XTAL2" x="-25.4" y="7.62" length="middle"/>
<pin name="PR1/XTAL1" x="-25.4" y="-7.62" length="middle"/>
<pin name="PA0/AREF" x="25.4" y="66.04" length="middle" rot="R180"/>
<pin name="PA1" x="25.4" y="63.5" length="middle" rot="R180"/>
<pin name="PA2" x="25.4" y="60.96" length="middle" rot="R180"/>
<pin name="PA3" x="25.4" y="58.42" length="middle" rot="R180"/>
<pin name="PA4" x="25.4" y="55.88" length="middle" rot="R180"/>
<pin name="PA5" x="25.4" y="53.34" length="middle" rot="R180"/>
<pin name="PA6" x="25.4" y="50.8" length="middle" rot="R180"/>
<pin name="PA7" x="25.4" y="48.26" length="middle" rot="R180"/>
<pin name="PB0/AREF" x="25.4" y="43.18" length="middle" rot="R180"/>
<pin name="PB1" x="25.4" y="40.64" length="middle" rot="R180"/>
<pin name="PB2/DAC0" x="25.4" y="38.1" length="middle" rot="R180"/>
<pin name="PB3/DAC1" x="25.4" y="35.56" length="middle" rot="R180"/>
<pin name="PB4" x="25.4" y="33.02" length="middle" rot="R180"/>
<pin name="PB5" x="25.4" y="30.48" length="middle" rot="R180"/>
<pin name="PB6" x="25.4" y="27.94" length="middle" rot="R180"/>
<pin name="PB7" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="PC0/SDA" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PC1/SCL/XCK0" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PC2/RXD0" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PC3/TXD0" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PC4/SS" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PC5/XCK1/MOSI" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="PC6/RXD1/MISO" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="PC7/TXD1/SCK" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="PD0" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PD1/XCK0" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PD2/RXD0" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PD3/TXD0" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PD4/SS" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PD5/XCK/MOSI" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PD6/RXD1/MISO/D-" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="PD7/TXD1/SCK/D+" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="PE0/SDA" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="PE1/SCL/XCK0" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="PE2/RXD0" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="PE3/TXD0" x="25.4" y="-33.02" length="middle" rot="R180"/>
<pin name="PE4/SS" x="25.4" y="-35.56" length="middle" rot="R180"/>
<pin name="PE5/XCK1/MOSI" x="25.4" y="-38.1" length="middle" rot="R180"/>
<pin name="PE6/RXD1/MISO" x="25.4" y="-40.64" length="middle" rot="R180"/>
<pin name="PE7/TXD1/SCK" x="25.4" y="-43.18" length="middle" rot="R180"/>
<pin name="PF0" x="25.4" y="-48.26" length="middle" rot="R180"/>
<pin name="PF1/XCK0" x="25.4" y="-50.8" length="middle" rot="R180"/>
<pin name="PF2/RXD0" x="25.4" y="-53.34" length="middle" rot="R180"/>
<pin name="PF3/TXD0" x="25.4" y="-55.88" length="middle" rot="R180"/>
<pin name="PF4" x="25.4" y="-58.42" length="middle" rot="R180"/>
<pin name="PF5" x="25.4" y="-60.96" length="middle" rot="R180"/>
<pin name="PF6" x="25.4" y="-63.5" length="middle" rot="R180"/>
<pin name="PF7" x="25.4" y="-66.04" length="middle" rot="R180"/>
<wire x1="20.32" y1="68.58" x2="20.32" y2="-68.58" width="0.254" layer="94"/>
<wire x1="20.32" y1="-68.58" x2="-20.32" y2="-68.58" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-68.58" x2="-20.32" y2="68.58" width="0.254" layer="94"/>
<wire x1="-20.32" y1="68.58" x2="20.32" y2="68.58" width="0.254" layer="94"/>
</symbol>
<symbol name="PDI">
<description>&lt;h3&gt;6 Pin Connection&lt;/h3&gt;
3x2 pin layout</description>
<pin name="PDI_DATA" x="-15.24" y="2.54" length="middle"/>
<pin name="NC1" x="-15.24" y="0" length="middle"/>
<pin name="PDI_CLK" x="-15.24" y="-2.54" length="middle"/>
<pin name="GND" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="NC2" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="2.54" length="middle" rot="R180"/>
<text x="-3.556" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-3.302" y="-5.842" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<wire x1="-11.43" y1="5.08" x2="-11.43" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="-11.43" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="11.43" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-11.43" y1="5.08" x2="11.43" y2="5.08" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATXMEGA_A3U" prefix="U">
<gates>
<gate name="G$1" symbol="ATXMEGA_A3U" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP" package="TQFP64_14X14MM-013THIN">
<connects>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND" pad="14 24 34 44 52 60"/>
<connect gate="G$1" pin="PA0/AREF" pad="62"/>
<connect gate="G$1" pin="PA1" pad="63"/>
<connect gate="G$1" pin="PA2" pad="64"/>
<connect gate="G$1" pin="PA3" pad="1"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0/AREF" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2/DAC0" pad="8"/>
<connect gate="G$1" pin="PB3/DAC1" pad="9"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="11"/>
<connect gate="G$1" pin="PB6" pad="12"/>
<connect gate="G$1" pin="PB7" pad="13"/>
<connect gate="G$1" pin="PC0/SDA" pad="16"/>
<connect gate="G$1" pin="PC1/SCL/XCK0" pad="17"/>
<connect gate="G$1" pin="PC2/RXD0" pad="18"/>
<connect gate="G$1" pin="PC3/TXD0" pad="19"/>
<connect gate="G$1" pin="PC4/SS" pad="20"/>
<connect gate="G$1" pin="PC5/XCK1/MOSI" pad="21"/>
<connect gate="G$1" pin="PC6/RXD1/MISO" pad="22"/>
<connect gate="G$1" pin="PC7/TXD1/SCK" pad="23"/>
<connect gate="G$1" pin="PD0" pad="26"/>
<connect gate="G$1" pin="PD1/XCK0" pad="27"/>
<connect gate="G$1" pin="PD2/RXD0" pad="28"/>
<connect gate="G$1" pin="PD3/TXD0" pad="29"/>
<connect gate="G$1" pin="PD4/SS" pad="30"/>
<connect gate="G$1" pin="PD5/XCK/MOSI" pad="31"/>
<connect gate="G$1" pin="PD6/RXD1/MISO/D-" pad="32"/>
<connect gate="G$1" pin="PD7/TXD1/SCK/D+" pad="33"/>
<connect gate="G$1" pin="PDI_DATA" pad="56"/>
<connect gate="G$1" pin="PE0/SDA" pad="36"/>
<connect gate="G$1" pin="PE1/SCL/XCK0" pad="37"/>
<connect gate="G$1" pin="PE2/RXD0" pad="38"/>
<connect gate="G$1" pin="PE3/TXD0" pad="39"/>
<connect gate="G$1" pin="PE4/SS" pad="40"/>
<connect gate="G$1" pin="PE5/XCK1/MOSI" pad="41"/>
<connect gate="G$1" pin="PE6/RXD1/MISO" pad="42"/>
<connect gate="G$1" pin="PE7/TXD1/SCK" pad="43"/>
<connect gate="G$1" pin="PF0" pad="46"/>
<connect gate="G$1" pin="PF1/XCK0" pad="47"/>
<connect gate="G$1" pin="PF2/RXD0" pad="48"/>
<connect gate="G$1" pin="PF3/TXD0" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0/XTAL2" pad="58"/>
<connect gate="G$1" pin="PR1/XTAL1" pad="59"/>
<connect gate="G$1" pin="RESET/PDI_CLK" pad="57"/>
<connect gate="G$1" pin="VCC" pad="15 25 35 45 53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="QFN" package="QFN-64-9X9MM">
<connects>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND" pad="14 24 34 44 52 60 P$1"/>
<connect gate="G$1" pin="PA0/AREF" pad="62"/>
<connect gate="G$1" pin="PA1" pad="63"/>
<connect gate="G$1" pin="PA2" pad="64"/>
<connect gate="G$1" pin="PA3" pad="1"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0/AREF" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2/DAC0" pad="8"/>
<connect gate="G$1" pin="PB3/DAC1" pad="9"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="11"/>
<connect gate="G$1" pin="PB6" pad="12"/>
<connect gate="G$1" pin="PB7" pad="13"/>
<connect gate="G$1" pin="PC0/SDA" pad="16"/>
<connect gate="G$1" pin="PC1/SCL/XCK0" pad="17"/>
<connect gate="G$1" pin="PC2/RXD0" pad="18"/>
<connect gate="G$1" pin="PC3/TXD0" pad="19"/>
<connect gate="G$1" pin="PC4/SS" pad="20"/>
<connect gate="G$1" pin="PC5/XCK1/MOSI" pad="21"/>
<connect gate="G$1" pin="PC6/RXD1/MISO" pad="22"/>
<connect gate="G$1" pin="PC7/TXD1/SCK" pad="23"/>
<connect gate="G$1" pin="PD0" pad="26"/>
<connect gate="G$1" pin="PD1/XCK0" pad="27"/>
<connect gate="G$1" pin="PD2/RXD0" pad="28"/>
<connect gate="G$1" pin="PD3/TXD0" pad="29"/>
<connect gate="G$1" pin="PD4/SS" pad="30"/>
<connect gate="G$1" pin="PD5/XCK/MOSI" pad="31"/>
<connect gate="G$1" pin="PD6/RXD1/MISO/D-" pad="32"/>
<connect gate="G$1" pin="PD7/TXD1/SCK/D+" pad="33"/>
<connect gate="G$1" pin="PDI_DATA" pad="56"/>
<connect gate="G$1" pin="PE0/SDA" pad="36"/>
<connect gate="G$1" pin="PE1/SCL/XCK0" pad="37"/>
<connect gate="G$1" pin="PE2/RXD0" pad="38"/>
<connect gate="G$1" pin="PE3/TXD0" pad="39"/>
<connect gate="G$1" pin="PE4/SS" pad="40"/>
<connect gate="G$1" pin="PE5/XCK1/MOSI" pad="41"/>
<connect gate="G$1" pin="PE6/RXD1/MISO" pad="42"/>
<connect gate="G$1" pin="PE7/TXD1/SCK" pad="43"/>
<connect gate="G$1" pin="PF0" pad="46"/>
<connect gate="G$1" pin="PF1/XCK0" pad="47"/>
<connect gate="G$1" pin="PF2/RXD0" pad="48"/>
<connect gate="G$1" pin="PF3/TXD0" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0/XTAL2" pad="58"/>
<connect gate="G$1" pin="PR1/XTAL1" pad="59"/>
<connect gate="G$1" pin="RESET/PDI_CLK" pad="57"/>
<connect gate="G$1" pin="VCC" pad="15 25 35 45 53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PDI" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12807"&gt; Header - 2x3 (Male, 0.1")&lt;/a&gt; (PRT-12807)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13010"&gt; Header - 2x3 (Female, 0.1")&lt;/a&gt; (PRT-13010)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10877"&gt; 2x3 Pin Shrouded Header&lt;/a&gt; (PRT-10877)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 3x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10651"&gt; Ribbon Crimp Connector - 6-pin (2x3, Female)&lt;/a&gt; (PRT-10651)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10646"&gt; Ribbon Cable - 6 wire (15ft)&lt;/a&gt; (PRT-10646)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="PDI" x="0" y="0"/>
</gates>
<devices>
<device name="FEMALE_SMD" package="PDI_2X3_SMD">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="NC1" pad="3"/>
<connect gate="G$1" pin="NC2" pad="4"/>
<connect gate="G$1" pin="PDI_CLK" pad="5"/>
<connect gate="G$1" pin="PDI_DATA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11290"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="RES_EFOBM">
<smd name="1" x="-1.35" y="0" dx="3.9" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="3.9" dy="0.8" layer="1" rot="R270"/>
<smd name="3" x="1.35" y="0" dx="3.9" dy="0.8" layer="1" rot="R270"/>
</package>
<package name="RES_PRQC">
<smd name="1" x="-0.95" y="0" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="1.5" dy="0.4" layer="1" rot="R270"/>
<smd name="3" x="0.95" y="0" dx="1.5" dy="0.6" layer="1" rot="R270"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.127" layer="51"/>
</package>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.032" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CERAMIC_RESONATOR">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="-0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="-1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-2.286" x2="1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.286" x2="2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.286" x2="-1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.286" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.302" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<circle x="-3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="0" y="-3.302" radius="0.254" width="0" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESONATOR" prefix="Y">
<gates>
<gate name="G$1" symbol="CERAMIC_RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RES_EFOBM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PQRC" package="RES_PRQC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="TSSOP16">
<description>&lt;b&gt;TSOP16&lt;/b&gt;&lt;p&gt;
thin small outline package</description>
<wire x1="3.1" y1="-2.4" x2="-3" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="3.1" y1="-2.4" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="-2.8" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.6" x2="-3" y2="0.5" width="0.2032" layer="21"/>
<wire x1="-3" y1="-0.5" x2="-3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.4" x2="-2.8" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-2.4" x2="3.1" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.1" y1="-2.4" x2="3.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="3.1" y1="2.6" x2="2.9" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="0.5" x2="-3" y2="-0.5" width="0.2032" layer="21" curve="-180"/>
<smd name="1" x="-2.225" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="2" x="-1.575" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="3" x="-0.925" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="4" x="-0.275" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="5" x="0.375" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="6" x="1.025" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="7" x="1.675" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="8" x="2.325" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="9" x="2.325" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="10" x="1.675" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="11" x="1.025" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="12" x="0.375" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="13" x="-0.275" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="14" x="-0.925" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="15" x="-1.575" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="16" x="-2.225" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<rectangle x1="-2.425" y1="-3.3501" x2="-2.0249" y2="-2.5299" layer="51"/>
<rectangle x1="-1.775" y1="-3.3501" x2="-1.3749" y2="-2.5299" layer="51"/>
<rectangle x1="-1.125" y1="-3.3501" x2="-0.725" y2="-2.5299" layer="51"/>
<rectangle x1="-0.475" y1="-3.3501" x2="-0.075" y2="-2.5299" layer="51"/>
<rectangle x1="0.175" y1="-3.3501" x2="0.575" y2="-2.5299" layer="51"/>
<rectangle x1="0.825" y1="-3.3501" x2="1.225" y2="-2.5299" layer="51"/>
<rectangle x1="1.4749" y1="-3.3501" x2="1.875" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="-3.3501" x2="2.525" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="2.7299" x2="2.525" y2="3.5501" layer="51"/>
<rectangle x1="1.4749" y1="2.7299" x2="1.875" y2="3.5501" layer="51"/>
<rectangle x1="0.825" y1="2.7299" x2="1.225" y2="3.5501" layer="51"/>
<rectangle x1="0.175" y1="2.7299" x2="0.575" y2="3.5501" layer="51"/>
<rectangle x1="-0.475" y1="2.7299" x2="-0.075" y2="3.5501" layer="51"/>
<rectangle x1="-1.125" y1="2.7299" x2="-0.725" y2="3.5501" layer="51"/>
<rectangle x1="-1.775" y1="2.7299" x2="-1.3749" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="2.7299" x2="-2.0249" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="-3.3501" x2="-2.0249" y2="-2.5299" layer="51"/>
<rectangle x1="-1.775" y1="-3.3501" x2="-1.3749" y2="-2.5299" layer="51"/>
<rectangle x1="-1.125" y1="-3.3501" x2="-0.725" y2="-2.5299" layer="51"/>
<rectangle x1="-0.475" y1="-3.3501" x2="-0.075" y2="-2.5299" layer="51"/>
<rectangle x1="0.175" y1="-3.3501" x2="0.575" y2="-2.5299" layer="51"/>
<rectangle x1="0.825" y1="-3.3501" x2="1.225" y2="-2.5299" layer="51"/>
<rectangle x1="1.4749" y1="-3.3501" x2="1.875" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="-3.3501" x2="2.525" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="2.7299" x2="2.525" y2="3.5501" layer="51"/>
<rectangle x1="1.4749" y1="2.7299" x2="1.875" y2="3.5501" layer="51"/>
<rectangle x1="0.825" y1="2.7299" x2="1.225" y2="3.5501" layer="51"/>
<rectangle x1="0.175" y1="2.7299" x2="0.575" y2="3.5501" layer="51"/>
<rectangle x1="-0.475" y1="2.7299" x2="-0.075" y2="3.5501" layer="51"/>
<rectangle x1="-1.125" y1="2.7299" x2="-0.725" y2="3.5501" layer="51"/>
<rectangle x1="-1.775" y1="2.7299" x2="-1.3749" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="2.7299" x2="-2.0249" y2="3.5501" layer="51"/>
<text x="-3.33375" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.96875" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-3.65125" y="-3.175" curve="-90"/>
<vertex x="-3.175" y="-2.69875" curve="-90"/>
<vertex x="-2.69875" y="-3.175" curve="-90"/>
<vertex x="-3.175" y="-3.65125" curve="-90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="RS485-2BY2-SN75C1168PWR">
<pin name="1B" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="1A" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="1R" x="-17.78" y="20.32" length="middle"/>
<pin name="1DE" x="-17.78" y="10.16" length="middle"/>
<pin name="2R" x="-17.78" y="-7.62" length="middle"/>
<pin name="2A" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="2B" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="-17.78" y="27.94" length="middle"/>
<pin name="2D" x="-17.78" y="-22.86" length="middle"/>
<pin name="2Y" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="2Z" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="2DE" x="-17.78" y="-17.78" length="middle"/>
<pin name="1Z" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="1Y" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="1D" x="-17.78" y="5.08" length="middle"/>
<pin name="VCC" x="-17.78" y="35.56" length="middle"/>
<text x="-2.54" y="38.1" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-27.94" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="-12.7" y1="38.1" x2="-12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="12.7" y2="38.1" width="0.254" layer="94"/>
<wire x1="12.7" y1="38.1" x2="-12.7" y2="38.1" width="0.254" layer="94"/>
<wire x1="-5.08" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="2.54" y2="22.86" width="0.254" layer="94"/>
<wire x1="2.54" y1="22.86" x2="2.54" y2="17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="17.78" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-17.78" x2="2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="0" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="0" y2="-20.32" width="0.254" layer="94"/>
<wire x1="0" y1="-20.32" x2="0" y2="-17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-22.86" x2="-2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-22.86" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="0" y1="-20.32" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-17.78" x2="0" y2="-17.78" width="0.254" layer="94"/>
<circle x="3.302" y="18.288" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="5.334" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="-5.334" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="-18.034" radius="0.254" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS485-2BY2-SN75C1168PWR" prefix="U">
<gates>
<gate name="G$1" symbol="RS485-2BY2-SN75C1168PWR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP16">
<connects>
<connect gate="G$1" pin="1A" pad="2"/>
<connect gate="G$1" pin="1B" pad="1"/>
<connect gate="G$1" pin="1D" pad="15"/>
<connect gate="G$1" pin="1DE" pad="4"/>
<connect gate="G$1" pin="1R" pad="3"/>
<connect gate="G$1" pin="1Y" pad="14"/>
<connect gate="G$1" pin="1Z" pad="13"/>
<connect gate="G$1" pin="2A" pad="6"/>
<connect gate="G$1" pin="2B" pad="7"/>
<connect gate="G$1" pin="2D" pad="9"/>
<connect gate="G$1" pin="2DE" pad="12"/>
<connect gate="G$1" pin="2R" pad="5"/>
<connect gate="G$1" pin="2Y" pad="10"/>
<connect gate="G$1" pin="2Z" pad="11"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="RJ45-LED-RJE7318800XXX">
<hole x="-5.74" y="0" drill="1.8"/>
<hole x="5.74" y="0" drill="1.8"/>
<pad name="SHDL1" x="-8" y="-4.35" drill="2" shape="square" rot="R180"/>
<pad name="SHDL2" x="8" y="-4.35" drill="2" shape="square"/>
<wire x1="8" y1="-6.5" x2="-8" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-8" y1="-6.5" x2="-8" y2="-9" width="0.1524" layer="51"/>
<wire x1="-8" y1="-9" x2="8" y2="-9" width="0.1524" layer="51"/>
<wire x1="8" y1="-9" x2="8" y2="6" width="0.1524" layer="51"/>
<wire x1="8" y1="6" x2="-8" y2="6" width="0.1524" layer="51"/>
<wire x1="-8" y1="6" x2="-8" y2="-6.5" width="0.1524" layer="51"/>
<pad name="P$1" x="-3.57" y="0.38" drill="0.9"/>
<pad name="P$2" x="-2.55" y="-1.45" drill="0.9"/>
<pad name="P$3" x="-1.53" y="0.38" drill="0.9"/>
<pad name="P$5" x="0.51" y="0.38" drill="0.9"/>
<pad name="P$7" x="2.55" y="0.38" drill="0.9"/>
<pad name="P$4" x="-0.51" y="-1.45" drill="0.9"/>
<pad name="P$6" x="1.53" y="-1.45" drill="0.9"/>
<pad name="P$8" x="3.57" y="-1.45" drill="0.9"/>
<pad name="YLWP" x="-7.01" y="3.6" drill="1.1"/>
<pad name="GRNN" x="7.01" y="3.6" drill="1.1"/>
<pad name="GRNP" x="4.47" y="3.6" drill="1.1"/>
<pad name="YLWN" x="-4.47" y="3.6" drill="1.1"/>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<circle x="0" y="0" radius="0.4953" width="0" layer="51"/>
</package>
<package name="SJFAB">
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="RJ45-LED-RJHSE-5381-UPTAB">
<hole x="-6.35" y="-2.54" drill="3.2512"/>
<hole x="6.35" y="-2.54" drill="3.2512"/>
<pad name="SHDL1" x="-8.128" y="0.889" drill="1.5748" shape="square" rot="R180"/>
<pad name="SHDL2" x="8.128" y="0.889" drill="1.5748" shape="square"/>
<wire x1="-8.254" y1="-6.5" x2="-8.255" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="-8.509" x2="8.255" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="8.255" y1="-8.509" x2="8.255" y2="7.366" width="0.1524" layer="51"/>
<wire x1="8.255" y1="7.366" x2="-8.255" y2="7.366" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="7.366" x2="-8.254" y2="-6.5" width="0.1524" layer="51"/>
<pad name="P$1" x="3.556" y="0" drill="0.9"/>
<pad name="P$2" x="2.54" y="1.778" drill="0.9"/>
<pad name="P$3" x="1.524" y="0" drill="0.9"/>
<pad name="P$5" x="-0.508" y="0" drill="0.9"/>
<pad name="P$7" x="-2.54" y="0" drill="0.9"/>
<pad name="P$4" x="0.508" y="1.778" drill="0.9"/>
<pad name="P$6" x="-1.524" y="1.778" drill="0.9"/>
<pad name="P$8" x="-3.556" y="1.778" drill="0.9"/>
<pad name="YLWP" x="-6.858" y="6.604" drill="0.889"/>
<pad name="GRNN" x="6.858" y="6.604" drill="0.889"/>
<pad name="GRNP" x="4.572" y="6.604" drill="0.889"/>
<pad name="YLWN" x="-4.572" y="6.604" drill="0.889"/>
</package>
</packages>
<symbols>
<symbol name="ATK-RS48PHYVE">
<pin name="3V3-LED" x="-10.16" y="-15.24" length="middle"/>
<pin name="LED-GRN-CATHODE" x="-10.16" y="-20.32" length="middle"/>
<pin name="LED-YLW-CATHODE" x="-10.16" y="-30.48" length="middle"/>
<pin name="CLKRX-Y" x="-10.16" y="-7.62" length="middle"/>
<pin name="CLKTX-Z" x="-10.16" y="-2.54" length="middle"/>
<pin name="RX-A" x="-10.16" y="2.54" length="middle"/>
<pin name="RX-B" x="-10.16" y="12.7" length="middle"/>
<pin name="TX-Z" x="-10.16" y="20.32" length="middle"/>
<pin name="TX-Y" x="-10.16" y="25.4" length="middle"/>
<pin name="CLKRX-B" x="-10.16" y="30.48" length="middle"/>
<pin name="CLKRX-A" x="-10.16" y="40.64" length="middle"/>
<wire x1="-5.08" y1="43.18" x2="-5.08" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-33.02" x2="20.32" y2="-33.02" width="0.254" layer="94"/>
<wire x1="20.32" y1="-33.02" x2="20.32" y2="43.18" width="0.254" layer="94"/>
<wire x1="20.32" y1="43.18" x2="-5.08" y2="43.18" width="0.254" layer="94"/>
<text x="0" y="45.72" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="-38.1" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SJFAB">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATK-RS48PHYVE" prefix="J">
<gates>
<gate name="G$1" symbol="ATK-RS48PHYVE" x="0" y="0"/>
</gates>
<devices>
<device name="RJ45LED" package="RJ45-LED-RJE7318800XXX">
<connects>
<connect gate="G$1" pin="3V3-LED" pad="GRNP YLWP"/>
<connect gate="G$1" pin="CLKRX-A" pad="P$8"/>
<connect gate="G$1" pin="CLKRX-B" pad="P$7"/>
<connect gate="G$1" pin="CLKRX-Y" pad="P$1"/>
<connect gate="G$1" pin="CLKTX-Z" pad="P$2"/>
<connect gate="G$1" pin="LED-GRN-CATHODE" pad="GRNN"/>
<connect gate="G$1" pin="LED-YLW-CATHODE" pad="YLWN"/>
<connect gate="G$1" pin="RX-A" pad="P$3"/>
<connect gate="G$1" pin="RX-B" pad="P$4"/>
<connect gate="G$1" pin="TX-Y" pad="P$6"/>
<connect gate="G$1" pin="TX-Z" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RJ45LEDTABUP" package="RJ45-LED-RJHSE-5381-UPTAB">
<connects>
<connect gate="G$1" pin="3V3-LED" pad="GRNP YLWP"/>
<connect gate="G$1" pin="CLKRX-A" pad="P$8"/>
<connect gate="G$1" pin="CLKRX-B" pad="P$7"/>
<connect gate="G$1" pin="CLKRX-Y" pad="P$1"/>
<connect gate="G$1" pin="CLKTX-Z" pad="P$2"/>
<connect gate="G$1" pin="LED-GRN-CATHODE" pad="GRNN"/>
<connect gate="G$1" pin="LED-YLW-CATHODE" pad="YLWN"/>
<connect gate="G$1" pin="RX-A" pad="P$3"/>
<connect gate="G$1" pin="RX-B" pad="P$4"/>
<connect gate="G$1" pin="TX-Y" pad="P$6"/>
<connect gate="G$1" pin="TX-Z" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="J">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER" prefix="J">
<gates>
<gate name="G$1" symbol="SJFAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJFAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X16_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole -16 Pin No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:16&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_16&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="13" x="30.48" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="14" x="33.02" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="15" x="35.56" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="16" x="38.1" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="32.766" y1="-0.254" x2="33.274" y2="0.254" layer="51"/>
<rectangle x1="30.226" y1="-0.254" x2="30.734" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="35.306" y1="-0.254" x2="35.814" y2="0.254" layer="51"/>
<rectangle x1="37.846" y1="-0.254" x2="38.354" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X16">
<description>&lt;h3&gt;Plated Through Hole -16 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:16&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_16&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0.635" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="33.655" y2="1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="1.27" x2="34.29" y2="0.635" width="0.2032" layer="21"/>
<wire x1="34.29" y1="-0.635" x2="33.655" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="1.27" x2="31.115" y2="1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="1.27" x2="31.75" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="31.115" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="-1.27" x2="29.845" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="-1.27" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.75" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="-1.27" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.67" y1="0.635" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="26.67" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0.635" x2="22.225" y2="1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="34.29" y2="0.635" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="36.195" y2="1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="1.27" x2="36.83" y2="0.635" width="0.2032" layer="21"/>
<wire x1="36.83" y1="-0.635" x2="36.195" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="-1.27" x2="34.925" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.29" y1="-0.635" x2="34.925" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="36.83" y2="0.635" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="38.735" y2="1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="1.27" x2="39.37" y2="0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0.635" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="-0.635" x2="38.735" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="-1.27" x2="37.465" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.83" y1="-0.635" x2="37.465" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="13" x="30.48" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="14" x="33.02" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="15" x="35.56" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="16" x="38.1" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="32.766" y1="-0.254" x2="33.274" y2="0.254" layer="51"/>
<rectangle x1="30.226" y1="-0.254" x2="30.734" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="35.306" y1="-0.254" x2="35.814" y2="0.254" layer="51"/>
<rectangle x1="37.846" y1="-0.254" x2="38.354" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X16_LOCK">
<description>&lt;h3&gt;Plated Through Hole -16 Pin Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" to hold pins in place while soldering.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:16&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_16&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="33.655" y2="1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="1.27" x2="34.29" y2="0.635" width="0.2032" layer="21"/>
<wire x1="34.29" y1="-0.635" x2="33.655" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="1.27" x2="31.115" y2="1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="1.27" x2="31.75" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="31.115" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="-1.27" x2="29.845" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="-1.27" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.75" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="-1.27" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.67" y1="0.635" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="26.67" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="0.635" x2="22.225" y2="1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="34.29" y2="0.635" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="36.195" y2="1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="1.27" x2="36.83" y2="0.635" width="0.2032" layer="21"/>
<wire x1="36.83" y1="-0.635" x2="36.195" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="-1.27" x2="34.925" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.29" y1="-0.635" x2="34.925" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="36.83" y2="0.635" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="38.735" y2="1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="1.27" x2="39.37" y2="0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0.635" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="-0.635" x2="38.735" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="-1.27" x2="37.465" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.83" y1="-0.635" x2="37.465" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="22.86" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="25.4" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="27.94" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="13" x="30.48" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="14" x="33.02" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="15" x="35.56" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="16" x="38.1" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="32.766" y1="-0.254" x2="33.274" y2="0.254" layer="51"/>
<rectangle x1="30.226" y1="-0.254" x2="30.734" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="35.306" y1="-0.254" x2="35.814" y2="0.254" layer="51"/>
<rectangle x1="37.846" y1="-0.254" x2="38.354" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X16_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole -16 Pin Locking Footprint w/ Long Pads&lt;/h3&gt;
Holes are offset 0.005" to hold pins in place while soldering.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:16&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_16&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<wire x1="16.764" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="19.304" y1="0" x2="18.796" y2="0" width="0.2032" layer="21"/>
<wire x1="21.844" y1="0" x2="21.336" y2="0" width="0.2032" layer="21"/>
<wire x1="24.384" y1="0" x2="23.876" y2="0" width="0.2032" layer="21"/>
<wire x1="26.924" y1="0" x2="26.416" y2="0" width="0.2032" layer="21"/>
<wire x1="29.464" y1="0" x2="28.956" y2="0" width="0.2032" layer="21"/>
<wire x1="32.004" y1="0" x2="31.496" y2="0" width="0.2032" layer="21"/>
<wire x1="34.544" y1="0" x2="34.036" y2="0" width="0.2032" layer="21"/>
<wire x1="37.084" y1="0" x2="36.576" y2="0" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0" x2="39.37" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="39.37" y1="-0.9906" x2="39.0906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0" x2="39.37" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0.9906" x2="39.0906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0" x2="39.116" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="20.32" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="22.86" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="25.4" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="27.94" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="13" x="30.48" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="14" x="33.02" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="15" x="35.56" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="16" x="38.1" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
<rectangle x1="17.4879" y1="-0.2921" x2="18.0721" y2="0.2921" layer="51"/>
<rectangle x1="20.0279" y1="-0.2921" x2="20.6121" y2="0.2921" layer="51"/>
<rectangle x1="22.5679" y1="-0.2921" x2="23.1521" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="25.1079" y1="-0.2921" x2="25.6921" y2="0.2921" layer="51"/>
<rectangle x1="27.6479" y1="-0.2921" x2="28.2321" y2="0.2921" layer="51"/>
<rectangle x1="30.1879" y1="-0.2921" x2="30.7721" y2="0.2921" layer="51"/>
<rectangle x1="32.7279" y1="-0.2921" x2="33.3121" y2="0.2921" layer="51"/>
<rectangle x1="35.2679" y1="-0.2921" x2="35.8521" y2="0.2921" layer="51"/>
<rectangle x1="37.8079" y1="-0.2921" x2="38.3921" y2="0.2921" layer="51" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X16_MACHINE-PIN-HEADER_LOCK.004">
<description>&lt;h3&gt;Plated Through Hole -16 Pin Machine Pin Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" to hold pins in place while soldering.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:16&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_16&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.67" y1="0.635" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="28.575" y2="1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="-1.27" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="26.67" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.13" y2="0.635" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.13" y1="0.635" x2="24.765" y2="1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="26.67" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.67" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="-1.27" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="-1.27" x2="24.13" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="-1.27" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="21.59" y2="0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="-1.27" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="17.145" y2="1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="-1.27" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.83" y1="0.635" x2="37.465" y2="1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="1.27" x2="38.735" y2="1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="1.27" x2="39.37" y2="0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="-0.635" x2="38.735" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="38.735" y1="-1.27" x2="37.465" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="37.465" y1="-1.27" x2="36.83" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="33.655" y2="1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="1.27" x2="34.29" y2="0.635" width="0.2032" layer="21"/>
<wire x1="34.29" y1="-0.635" x2="33.655" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.29" y1="0.635" x2="34.925" y2="1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="1.27" x2="36.195" y2="1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="1.27" x2="36.83" y2="0.635" width="0.2032" layer="21"/>
<wire x1="36.83" y1="-0.635" x2="36.195" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="36.195" y1="-1.27" x2="34.925" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="34.925" y1="-1.27" x2="34.29" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="1.27" x2="31.115" y2="1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="1.27" x2="31.75" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="31.115" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="-1.27" x2="29.845" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="-1.27" x2="29.21" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.75" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.75" y1="-0.635" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="33.655" y1="-1.27" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.21" y2="0.635" width="0.2032" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0.635" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="2.54" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="5.08" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="7.62" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="10.16" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="12.7" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="15.24" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="17.78" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="20.32" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="22.86" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="25.4" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="27.94" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="30.48" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="33.02" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="35.56" y="0" radius="0.3302" width="0.0254" layer="51"/>
<circle x="38.1" y="0" radius="0.3302" width="0.0254" layer="51"/>
<pad name="1" x="0" y="0.1016" drill="0.889" rot="R90"/>
<pad name="2" x="2.54" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="3" x="5.08" y="0.1016" drill="0.889" rot="R90"/>
<pad name="4" x="7.62" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="5" x="10.16" y="0.1016" drill="0.889" rot="R90"/>
<pad name="6" x="12.7" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="7" x="15.24" y="0.1016" drill="0.889" rot="R90"/>
<pad name="8" x="17.78" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="9" x="20.32" y="0.1016" drill="0.889" rot="R90"/>
<pad name="10" x="22.86" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="11" x="25.4" y="0.1016" drill="0.889" rot="R90"/>
<pad name="12" x="27.94" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="13" x="30.48" y="0.1016" drill="0.889" rot="R90"/>
<pad name="14" x="33.02" y="-0.1016" drill="0.889" rot="R90"/>
<pad name="15" x="35.56" y="0.1016" drill="0.889" rot="R90"/>
<pad name="16" x="38.1" y="-0.1016" drill="0.889" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X16_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole -16 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:16&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_16&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="39.37" y1="0.635" x2="39.37" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="9" x="20.32" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="10" x="22.86" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="11" x="25.4" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="12" x="27.94" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="13" x="30.48" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="14" x="33.02" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="15" x="35.56" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="16" x="38.1" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="32.766" y1="-0.254" x2="33.274" y2="0.254" layer="51"/>
<rectangle x1="30.226" y1="-0.254" x2="30.734" y2="0.254" layer="51"/>
<rectangle x1="27.686" y1="-0.254" x2="28.194" y2="0.254" layer="51"/>
<rectangle x1="25.146" y1="-0.254" x2="25.654" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="35.306" y1="-0.254" x2="35.814" y2="0.254" layer="51"/>
<rectangle x1="37.846" y1="-0.254" x2="38.354" y2="0.254" layer="51"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CONN_16">
<description>&lt;h3&gt; 16 Pin Connection&lt;/h3&gt;</description>
<wire x1="6.35" y1="-22.86" x2="0" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="5.08" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-17.78" x2="5.08" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-20.32" x2="5.08" y2="-20.32" width="0.6096" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="-22.86" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-22.86" x2="6.35" y2="20.32" width="0.4064" layer="94"/>
<wire x1="0" y1="20.32" x2="6.35" y2="20.32" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="5.08" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="5.08" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="5.08" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<wire x1="3.81" y1="12.7" x2="5.08" y2="12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="15.24" x2="5.08" y2="15.24" width="0.6096" layer="94"/>
<wire x1="3.81" y1="17.78" x2="5.08" y2="17.78" width="0.6096" layer="94"/>
<text x="0" y="-25.146" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="0" y="20.828" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="10.16" y="-20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="10.16" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="10.16" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="14" x="10.16" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="10.16" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="16" x="10.16" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_16" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208
&lt;p&gt;&lt;/p&gt;
NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.</description>
<gates>
<gate name="G$1" symbol="CONN_16" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X16">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X16_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X16_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MACHIINE-PIN_LOCK" package="1X16_MACHINE-PIN-HEADER_LOCK.004">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X16_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X16_NO_SILK" package="1X16_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J2" library="power" deviceset="PWRPAD" device="M3-STANDOFF"/>
<part name="J1" library="power" deviceset="PWRPAD" device="M3-STANDOFF"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C8" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C7" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C6" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C5" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C4" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D3" library="lights" deviceset="LED" device="0805"/>
<part name="R3" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D2" library="lights" deviceset="LED" device="0805"/>
<part name="R2" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D1" library="lights" deviceset="LED" device="0805"/>
<part name="R1" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="+3V33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C2" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J11" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J13" library="power" deviceset="PWRPAD" device="M3"/>
<part name="U2" library="microcontrollers" deviceset="ATXMEGA_A3U" device="QFN"/>
<part name="Y1" library="passives" deviceset="RESONATOR" device="PQRC"/>
<part name="C3" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="J3" library="microcontrollers" deviceset="PDI" device="FEMALE_SMD"/>
<part name="+3V32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="S1" library="passives" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="U1" library="power" deviceset="BUCK-TS30011-12-13" device="QFN"/>
<part name="C9" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="L1" library="power" deviceset="INDUCTOR" device="" value="4.7uH"/>
<part name="C1" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C10" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C11" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U4" library="comm" deviceset="RS485-2BY2-SN75C1168PWR" device=""/>
<part name="J10" library="connector" deviceset="ATK-RS48PHYVE" device="RJ45LEDTABUP" value="ATK-RS48PHYVERJ45LEDTABUP"/>
<part name="C16" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R6" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="R7" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="+3V35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R4" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R5" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="C12" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C14" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="U3" library="power" deviceset="VREG-AP2112" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="+3V34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C13" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C15" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="J8" library="SparkFun-Connectors" deviceset="CONN_16" device="1X16_NO_SILK"/>
<part name="J7" library="SparkFun-Connectors" deviceset="CONN_16" device="1X16_NO_SILK"/>
<part name="J6" library="SparkFun-Connectors" deviceset="CONN_16" device="1X16_NO_SILK"/>
<part name="J15" library="SparkFun-Connectors" deviceset="CONN_16" device="1X16_NO_SILK"/>
<part name="J16" library="SparkFun-Connectors" deviceset="CONN_16" device="1X16_NO_SILK"/>
<part name="J4" library="connector" deviceset="FIDUCIAL" device=""/>
<part name="J5" library="connector" deviceset="FIDUCIAL" device=""/>
<part name="J9" library="SparkFun-Connectors" deviceset="CONN_16" device="1X16_NO_SILK"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="J14" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="J12" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C17" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D4" library="passives" deviceset="DIODE" device="SOD123HE"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="J2" gate="G$1" x="-48.26" y="238.76" rot="R270"/>
<instance part="J1" gate="G$1" x="-48.26" y="266.7" rot="R90"/>
<instance part="P+1" gate="VCC" x="-53.34" y="261.62" rot="R90"/>
<instance part="GND1" gate="1" x="-53.34" y="243.84" rot="R270"/>
<instance part="C8" gate="G$1" x="25.4" y="147.32"/>
<instance part="C7" gate="G$1" x="15.24" y="147.32"/>
<instance part="C6" gate="G$1" x="5.08" y="147.32"/>
<instance part="C5" gate="G$1" x="-5.08" y="147.32"/>
<instance part="C4" gate="G$1" x="-15.24" y="147.32"/>
<instance part="GND3" gate="1" x="-27.94" y="172.72" rot="R270"/>
<instance part="GND2" gate="1" x="-45.72" y="144.78" rot="R270"/>
<instance part="+3V31" gate="G$1" x="-45.72" y="152.4" rot="R90"/>
<instance part="D3" gate="G$1" x="60.96" y="281.94" rot="R270"/>
<instance part="R3" gate="G$1" x="48.26" y="281.94"/>
<instance part="D2" gate="G$1" x="60.96" y="292.1" rot="R270"/>
<instance part="R2" gate="G$1" x="48.26" y="292.1"/>
<instance part="D1" gate="G$1" x="60.96" y="302.26" rot="R270"/>
<instance part="R1" gate="G$1" x="48.26" y="302.26"/>
<instance part="+3V33" gate="G$1" x="83.82" y="287.02" rot="R270"/>
<instance part="GND5" gate="1" x="30.48" y="302.26" rot="R270"/>
<instance part="C2" gate="G$1" x="-30.48" y="256.54"/>
<instance part="GND4" gate="1" x="15.24" y="106.68" rot="R270"/>
<instance part="J11" gate="G$1" x="205.74" y="154.94" rot="R90"/>
<instance part="J13" gate="G$1" x="205.74" y="81.28" rot="R270"/>
<instance part="U2" gate="G$1" x="63.5" y="106.68"/>
<instance part="Y1" gate="G$1" x="30.48" y="106.68" rot="R270"/>
<instance part="C3" gate="G$1" x="-25.4" y="147.32"/>
<instance part="J3" gate="G$1" x="5.08" y="170.18" rot="R180"/>
<instance part="+3V32" gate="G$1" x="-27.94" y="167.64" rot="R90"/>
<instance part="S1" gate="G$1" x="5.08" y="182.88" rot="R270"/>
<instance part="U1" gate="G$1" x="5.08" y="251.46"/>
<instance part="C9" gate="G$1" x="27.94" y="256.54"/>
<instance part="L1" gate="G$1" x="45.72" y="254" rot="R270"/>
<instance part="C1" gate="G$1" x="-40.64" y="256.54"/>
<instance part="C10" gate="G$1" x="60.96" y="248.92"/>
<instance part="C11" gate="G$1" x="73.66" y="248.92"/>
<instance part="GND7" gate="1" x="109.22" y="246.38" rot="R90"/>
<instance part="U4" gate="G$1" x="167.64" y="228.6"/>
<instance part="J10" gate="G$1" x="203.2" y="213.36"/>
<instance part="C16" gate="G$1" x="139.7" y="259.08"/>
<instance part="GND8" gate="1" x="124.46" y="256.54" rot="R270"/>
<instance part="P+3" gate="1" x="124.46" y="264.16" rot="R90"/>
<instance part="R6" gate="G$1" x="187.96" y="248.92" rot="R90"/>
<instance part="R7" gate="G$1" x="187.96" y="220.98" rot="R90"/>
<instance part="+3V35" gate="G$1" x="124.46" y="198.12" rot="R90"/>
<instance part="R4" gate="G$1" x="175.26" y="193.04"/>
<instance part="R5" gate="G$1" x="175.26" y="182.88"/>
<instance part="C12" gate="G$1" x="83.82" y="248.92"/>
<instance part="C14" gate="G$1" x="93.98" y="248.92"/>
<instance part="U3" gate="G$1" x="73.66" y="226.06"/>
<instance part="P+2" gate="1" x="109.22" y="254" rot="R270"/>
<instance part="+3V34" gate="G$1" x="109.22" y="228.6" rot="R270"/>
<instance part="GND6" gate="1" x="73.66" y="208.28"/>
<instance part="C13" gate="G$1" x="88.9" y="223.52"/>
<instance part="C15" gate="G$1" x="99.06" y="223.52"/>
<instance part="J8" gate="G$1" x="157.48" y="111.76" rot="R180"/>
<instance part="J7" gate="G$1" x="144.78" y="111.76" rot="R180"/>
<instance part="J6" gate="G$1" x="132.08" y="111.76" rot="R180"/>
<instance part="J15" gate="G$1" x="243.84" y="111.76" rot="R180"/>
<instance part="J16" gate="G$1" x="261.62" y="111.76" rot="R180"/>
<instance part="J4" gate="G$1" x="10.16" y="7.62"/>
<instance part="J5" gate="G$1" x="20.32" y="7.62"/>
<instance part="J9" gate="G$1" x="195.58" y="111.76" rot="R180"/>
<instance part="GND10" gate="1" x="248.92" y="71.12"/>
<instance part="+3V36" gate="G$1" x="205.74" y="129.54"/>
<instance part="P+4" gate="1" x="215.9" y="144.78"/>
<instance part="J14" gate="G$1" x="215.9" y="134.62" rot="R90"/>
<instance part="J12" gate="G$1" x="205.74" y="119.38" rot="R90"/>
<instance part="P+5" gate="VCC" x="226.06" y="154.94"/>
<instance part="C17" gate="G$1" x="215.9" y="88.9"/>
<instance part="GND9" gate="1" x="215.9" y="81.28"/>
<instance part="D4" gate="G$1" x="27.94" y="246.38" rot="R90"/>
<instance part="GND11" gate="1" x="27.94" y="233.68"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="43.18" y1="302.26" x2="33.02" y2="302.26" width="0.1524" layer="91"/>
<label x="33.02" y="302.26" size="1.778" layer="95"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="17.78" y1="106.68" x2="22.86" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="38.1" y1="144.78" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="25.4" y1="144.78" x2="15.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="25.4" y="144.78"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="15.24" y1="144.78" x2="5.08" y2="144.78" width="0.1524" layer="91"/>
<junction x="15.24" y="144.78"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="5.08" y1="144.78" x2="-5.08" y2="144.78" width="0.1524" layer="91"/>
<junction x="5.08" y="144.78"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="144.78" x2="-15.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="-5.08" y="144.78"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="144.78" x2="-25.4" y2="144.78" width="0.1524" layer="91"/>
<junction x="-15.24" y="144.78"/>
<wire x1="-25.4" y1="144.78" x2="-43.18" y2="144.78" width="0.1524" layer="91"/>
<junction x="-25.4" y="144.78"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="J3" gate="G$1" pin="GND"/>
<wire x1="-25.4" y1="172.72" x2="-12.7" y2="172.72" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="P1"/>
<wire x1="-12.7" y1="172.72" x2="-10.16" y2="172.72" width="0.1524" layer="91"/>
<wire x1="0" y1="180.34" x2="-12.7" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="180.34" x2="-12.7" y2="172.72" width="0.1524" layer="91"/>
<junction x="-12.7" y="172.72"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="-10.16" y1="246.38" x2="-12.7" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="246.38" x2="-12.7" y2="243.84" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PGND"/>
<wire x1="-12.7" y1="243.84" x2="-10.16" y2="243.84" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-12.7" y1="243.84" x2="-30.48" y2="243.84" width="0.1524" layer="91"/>
<junction x="-12.7" y="243.84"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="243.84" x2="-40.64" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="243.84" x2="-48.26" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="243.84" x2="-50.8" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="254" x2="-30.48" y2="243.84" width="0.1524" layer="91"/>
<junction x="-30.48" y="243.84"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="254" x2="-40.64" y2="243.84" width="0.1524" layer="91"/>
<junction x="-40.64" y="243.84"/>
<pinref part="J2" gate="G$1" pin="PWRPAD"/>
<junction x="-48.26" y="243.84"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="60.96" y1="246.38" x2="73.66" y2="246.38" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="73.66" y1="246.38" x2="83.82" y2="246.38" width="0.1524" layer="91"/>
<junction x="73.66" y="246.38"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="83.82" y1="246.38" x2="93.98" y2="246.38" width="0.1524" layer="91"/>
<junction x="83.82" y="246.38"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="93.98" y1="246.38" x2="106.68" y2="246.38" width="0.1524" layer="91"/>
<junction x="93.98" y="246.38"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="149.86" y1="256.54" x2="139.7" y2="256.54" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="139.7" y1="256.54" x2="127" y2="256.54" width="0.1524" layer="91"/>
<junction x="139.7" y="256.54"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="73.66" y1="215.9" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
<wire x1="73.66" y1="213.36" x2="73.66" y2="210.82" width="0.1524" layer="91"/>
<wire x1="73.66" y1="213.36" x2="88.9" y2="213.36" width="0.1524" layer="91"/>
<junction x="73.66" y="213.36"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="88.9" y1="213.36" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="88.9" y1="213.36" x2="99.06" y2="213.36" width="0.1524" layer="91"/>
<wire x1="99.06" y1="213.36" x2="99.06" y2="220.98" width="0.1524" layer="91"/>
<junction x="88.9" y="213.36"/>
</segment>
<segment>
<pinref part="J16" gate="G$1" pin="1"/>
<wire x1="251.46" y1="132.08" x2="248.92" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="248.92" y1="132.08" x2="248.92" y2="129.54" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="2"/>
<wire x1="248.92" y1="129.54" x2="248.92" y2="127" width="0.1524" layer="91"/>
<wire x1="248.92" y1="127" x2="248.92" y2="124.46" width="0.1524" layer="91"/>
<wire x1="248.92" y1="124.46" x2="248.92" y2="121.92" width="0.1524" layer="91"/>
<wire x1="248.92" y1="121.92" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<wire x1="248.92" y1="119.38" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<wire x1="248.92" y1="116.84" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<wire x1="248.92" y1="114.3" x2="248.92" y2="111.76" width="0.1524" layer="91"/>
<wire x1="248.92" y1="111.76" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<wire x1="248.92" y1="109.22" x2="248.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="248.92" y1="106.68" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="248.92" y1="104.14" x2="248.92" y2="101.6" width="0.1524" layer="91"/>
<wire x1="248.92" y1="101.6" x2="248.92" y2="99.06" width="0.1524" layer="91"/>
<wire x1="248.92" y1="99.06" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="248.92" y1="96.52" x2="248.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="248.92" y1="93.98" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<wire x1="251.46" y1="129.54" x2="248.92" y2="129.54" width="0.1524" layer="91"/>
<junction x="248.92" y="129.54"/>
<pinref part="J16" gate="G$1" pin="3"/>
<wire x1="251.46" y1="127" x2="248.92" y2="127" width="0.1524" layer="91"/>
<junction x="248.92" y="127"/>
<pinref part="J16" gate="G$1" pin="4"/>
<wire x1="251.46" y1="124.46" x2="248.92" y2="124.46" width="0.1524" layer="91"/>
<junction x="248.92" y="124.46"/>
<pinref part="J16" gate="G$1" pin="5"/>
<wire x1="251.46" y1="121.92" x2="248.92" y2="121.92" width="0.1524" layer="91"/>
<junction x="248.92" y="121.92"/>
<pinref part="J16" gate="G$1" pin="6"/>
<wire x1="251.46" y1="119.38" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<junction x="248.92" y="119.38"/>
<pinref part="J16" gate="G$1" pin="7"/>
<wire x1="251.46" y1="116.84" x2="248.92" y2="116.84" width="0.1524" layer="91"/>
<junction x="248.92" y="116.84"/>
<pinref part="J16" gate="G$1" pin="8"/>
<wire x1="251.46" y1="114.3" x2="248.92" y2="114.3" width="0.1524" layer="91"/>
<junction x="248.92" y="114.3"/>
<pinref part="J16" gate="G$1" pin="9"/>
<wire x1="251.46" y1="111.76" x2="248.92" y2="111.76" width="0.1524" layer="91"/>
<junction x="248.92" y="111.76"/>
<pinref part="J16" gate="G$1" pin="10"/>
<wire x1="251.46" y1="109.22" x2="248.92" y2="109.22" width="0.1524" layer="91"/>
<junction x="248.92" y="109.22"/>
<pinref part="J16" gate="G$1" pin="11"/>
<wire x1="251.46" y1="106.68" x2="248.92" y2="106.68" width="0.1524" layer="91"/>
<junction x="248.92" y="106.68"/>
<pinref part="J16" gate="G$1" pin="12"/>
<wire x1="251.46" y1="104.14" x2="248.92" y2="104.14" width="0.1524" layer="91"/>
<junction x="248.92" y="104.14"/>
<pinref part="J16" gate="G$1" pin="13"/>
<wire x1="251.46" y1="101.6" x2="248.92" y2="101.6" width="0.1524" layer="91"/>
<junction x="248.92" y="101.6"/>
<pinref part="J16" gate="G$1" pin="14"/>
<wire x1="251.46" y1="99.06" x2="248.92" y2="99.06" width="0.1524" layer="91"/>
<junction x="248.92" y="99.06"/>
<pinref part="J16" gate="G$1" pin="15"/>
<wire x1="251.46" y1="96.52" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<junction x="248.92" y="96.52"/>
<pinref part="J16" gate="G$1" pin="16"/>
<wire x1="251.46" y1="93.98" x2="248.92" y2="93.98" width="0.1524" layer="91"/>
<junction x="248.92" y="93.98"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="215.9" y1="83.82" x2="215.9" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="27.94" y1="243.84" x2="27.94" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="66.04" y1="302.26" x2="76.2" y2="302.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="302.26" x2="76.2" y2="292.1" width="0.1524" layer="91"/>
<wire x1="76.2" y1="292.1" x2="76.2" y2="287.02" width="0.1524" layer="91"/>
<wire x1="76.2" y1="287.02" x2="76.2" y2="281.94" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="66.04" y1="281.94" x2="76.2" y2="281.94" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="66.04" y1="292.1" x2="76.2" y2="292.1" width="0.1524" layer="91"/>
<junction x="76.2" y="292.1"/>
<wire x1="76.2" y1="287.02" x2="81.28" y2="287.02" width="0.1524" layer="91"/>
<junction x="76.2" y="287.02"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="152.4" x2="-25.4" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="152.4" x2="-15.24" y2="152.4" width="0.1524" layer="91"/>
<junction x="-25.4" y="152.4"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="152.4" x2="-5.08" y2="152.4" width="0.1524" layer="91"/>
<junction x="-15.24" y="152.4"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="152.4" x2="5.08" y2="152.4" width="0.1524" layer="91"/>
<junction x="-5.08" y="152.4"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="5.08" y1="152.4" x2="15.24" y2="152.4" width="0.1524" layer="91"/>
<junction x="5.08" y="152.4"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="25.4" y1="152.4" x2="25.4" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="AVCC"/>
<wire x1="25.4" y1="157.48" x2="38.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="38.1" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<junction x="25.4" y="152.4"/>
<wire x1="15.24" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<junction x="15.24" y="152.4"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<pinref part="J3" gate="G$1" pin="VCC"/>
<wire x1="-25.4" y1="167.64" x2="-10.16" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<pinref part="J10" gate="G$1" pin="3V3-LED"/>
<wire x1="127" y1="198.12" x2="144.78" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="1DE"/>
<wire x1="144.78" y1="198.12" x2="193.04" y2="198.12" width="0.1524" layer="91"/>
<wire x1="149.86" y1="238.76" x2="144.78" y2="238.76" width="0.1524" layer="91"/>
<wire x1="144.78" y1="238.76" x2="144.78" y2="210.82" width="0.1524" layer="91"/>
<junction x="144.78" y="198.12"/>
<pinref part="U4" gate="G$1" pin="2DE"/>
<wire x1="144.78" y1="210.82" x2="144.78" y2="198.12" width="0.1524" layer="91"/>
<wire x1="149.86" y1="210.82" x2="144.78" y2="210.82" width="0.1524" layer="91"/>
<junction x="144.78" y="210.82"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VOUT"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="86.36" y1="228.6" x2="88.9" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="88.9" y1="228.6" x2="99.06" y2="228.6" width="0.1524" layer="91"/>
<junction x="88.9" y="228.6"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="99.06" y1="228.6" x2="106.68" y2="228.6" width="0.1524" layer="91"/>
<junction x="99.06" y="228.6"/>
</segment>
<segment>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<pinref part="J12" gate="G$1" pin="2"/>
<wire x1="205.74" y1="127" x2="205.74" y2="124.46" width="0.1524" layer="91"/>
<wire x1="205.74" y1="124.46" x2="208.28" y2="124.46" width="0.1524" layer="91"/>
<junction x="205.74" y="124.46"/>
<wire x1="208.28" y1="124.46" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="208.28" y1="114.3" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J15" gate="G$1" pin="9"/>
<wire x1="233.68" y1="111.76" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<wire x1="205.74" y1="111.76" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<junction x="205.74" y="114.3"/>
<wire x1="205.74" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<junction x="205.74" y="111.76"/>
<pinref part="J15" gate="G$1" pin="10"/>
<wire x1="205.74" y1="109.22" x2="233.68" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J15" gate="G$1" pin="11"/>
<wire x1="233.68" y1="106.68" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<wire x1="205.74" y1="106.68" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<junction x="205.74" y="109.22"/>
<pinref part="J15" gate="G$1" pin="12"/>
<wire x1="233.68" y1="104.14" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<wire x1="205.74" y1="104.14" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<junction x="205.74" y="106.68"/>
<pinref part="J15" gate="G$1" pin="13"/>
<wire x1="233.68" y1="101.6" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<wire x1="205.74" y1="101.6" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<junction x="205.74" y="104.14"/>
<pinref part="J15" gate="G$1" pin="14"/>
<wire x1="233.68" y1="99.06" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<wire x1="205.74" y1="99.06" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<junction x="205.74" y="101.6"/>
<pinref part="J15" gate="G$1" pin="15"/>
<wire x1="233.68" y1="96.52" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<wire x1="205.74" y1="96.52" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<junction x="205.74" y="99.06"/>
<pinref part="J15" gate="G$1" pin="16"/>
<wire x1="233.68" y1="93.98" x2="215.9" y2="93.98" width="0.1524" layer="91"/>
<wire x1="215.9" y1="93.98" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<wire x1="205.74" y1="93.98" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<junction x="205.74" y="96.52"/>
<pinref part="C17" gate="G$1" pin="1"/>
<junction x="215.9" y="93.98"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="53.34" y1="302.26" x2="58.42" y2="302.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="53.34" y1="292.1" x2="58.42" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="53.34" y1="281.94" x2="58.42" y2="281.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STLCLK" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="43.18" y1="292.1" x2="33.02" y2="292.1" width="0.1524" layer="91"/>
<label x="33.02" y="292.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PE2/RXD0"/>
<wire x1="88.9" y1="76.2" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="91.44" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="STLERR" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="43.18" y1="281.94" x2="33.02" y2="281.94" width="0.1524" layer="91"/>
<label x="33.02" y="281.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PE3/TXD0"/>
<wire x1="88.9" y1="73.66" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<label x="91.44" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<pinref part="U2" gate="G$1" pin="PR0/XTAL2"/>
<wire x1="30.48" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="3"/>
<pinref part="U2" gate="G$1" pin="PR1/XTAL1"/>
<wire x1="30.48" y1="99.06" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="PDI_CLK"/>
<pinref part="U2" gate="G$1" pin="RESET/PDI_CLK"/>
<wire x1="20.32" y1="172.72" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="S1"/>
<wire x1="22.86" y1="172.72" x2="38.1" y2="172.72" width="0.1524" layer="91"/>
<wire x1="10.16" y1="180.34" x2="22.86" y2="180.34" width="0.1524" layer="91"/>
<wire x1="22.86" y1="180.34" x2="22.86" y2="177.8" width="0.1524" layer="91"/>
<wire x1="22.86" y1="177.8" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
<junction x="22.86" y="172.72"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="PDI_DATA"/>
<pinref part="U2" gate="G$1" pin="PDI_DATA"/>
<wire x1="20.32" y1="167.64" x2="38.1" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="BST"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="20.32" y1="261.62" x2="27.94" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VSW"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="20.32" y1="254" x2="27.94" y2="254" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="254" x2="38.1" y2="254" width="0.1524" layer="91"/>
<junction x="27.94" y="254"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="27.94" y1="248.92" x2="27.94" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="261.62" x2="-48.26" y2="261.62" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="261.62" x2="-40.64" y2="261.62" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="261.62" x2="-30.48" y2="261.62" width="0.1524" layer="91"/>
<junction x="-40.64" y="261.62"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="-30.48" y1="261.62" x2="-10.16" y2="261.62" width="0.1524" layer="91"/>
<junction x="-30.48" y="261.62"/>
<pinref part="J1" gate="G$1" pin="PWRPAD"/>
<junction x="-48.26" y="261.62"/>
</segment>
<segment>
<pinref part="J15" gate="G$1" pin="4"/>
<wire x1="233.68" y1="124.46" x2="226.06" y2="124.46" width="0.1524" layer="91"/>
<wire x1="226.06" y1="124.46" x2="226.06" y2="127" width="0.1524" layer="91"/>
<pinref part="J15" gate="G$1" pin="3"/>
<wire x1="226.06" y1="127" x2="226.06" y2="129.54" width="0.1524" layer="91"/>
<wire x1="226.06" y1="129.54" x2="226.06" y2="132.08" width="0.1524" layer="91"/>
<wire x1="226.06" y1="132.08" x2="226.06" y2="152.4" width="0.1524" layer="91"/>
<wire x1="233.68" y1="127" x2="226.06" y2="127" width="0.1524" layer="91"/>
<junction x="226.06" y="127"/>
<pinref part="J15" gate="G$1" pin="2"/>
<wire x1="233.68" y1="129.54" x2="226.06" y2="129.54" width="0.1524" layer="91"/>
<junction x="226.06" y="129.54"/>
<pinref part="J15" gate="G$1" pin="1"/>
<wire x1="233.68" y1="132.08" x2="226.06" y2="132.08" width="0.1524" layer="91"/>
<junction x="226.06" y="132.08"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="127" y1="264.16" x2="139.7" y2="264.16" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="139.7" y1="264.16" x2="149.86" y2="264.16" width="0.1524" layer="91"/>
<junction x="139.7" y="264.16"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="FB"/>
<wire x1="20.32" y1="246.38" x2="55.88" y2="246.38" width="0.1524" layer="91"/>
<wire x1="55.88" y1="246.38" x2="55.88" y2="254" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="55.88" y1="254" x2="53.34" y2="254" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="55.88" y1="254" x2="58.42" y2="254" width="0.1524" layer="91"/>
<junction x="55.88" y="254"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="58.42" y1="254" x2="60.96" y2="254" width="0.1524" layer="91"/>
<wire x1="60.96" y1="254" x2="73.66" y2="254" width="0.1524" layer="91"/>
<junction x="60.96" y="254"/>
<wire x1="106.68" y1="254" x2="93.98" y2="254" width="0.1524" layer="91"/>
<junction x="73.66" y="254"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="93.98" y1="254" x2="83.82" y2="254" width="0.1524" layer="91"/>
<wire x1="83.82" y1="254" x2="73.66" y2="254" width="0.1524" layer="91"/>
<junction x="83.82" y="254"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="93.98" y="254"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<wire x1="60.96" y1="228.6" x2="58.42" y2="228.6" width="0.1524" layer="91"/>
<wire x1="58.42" y1="228.6" x2="58.42" y2="254" width="0.1524" layer="91"/>
<junction x="58.42" y="254"/>
<pinref part="U3" gate="G$1" pin="EN"/>
<wire x1="60.96" y1="223.52" x2="58.42" y2="223.52" width="0.1524" layer="91"/>
<wire x1="58.42" y1="223.52" x2="58.42" y2="228.6" width="0.1524" layer="91"/>
<junction x="58.42" y="228.6"/>
<pinref part="P+2" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="J14" gate="G$1" pin="2"/>
<wire x1="215.9" y1="142.24" x2="215.9" y2="139.7" width="0.1524" layer="91"/>
<wire x1="215.9" y1="139.7" x2="218.44" y2="139.7" width="0.1524" layer="91"/>
<junction x="215.9" y="139.7"/>
<wire x1="218.44" y1="139.7" x2="218.44" y2="129.54" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="1"/>
<wire x1="218.44" y1="129.54" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="129.54" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<junction x="215.9" y="129.54"/>
<pinref part="J15" gate="G$1" pin="5"/>
<wire x1="215.9" y1="121.92" x2="233.68" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J15" gate="G$1" pin="6"/>
<wire x1="233.68" y1="119.38" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<wire x1="215.9" y1="119.38" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<junction x="215.9" y="121.92"/>
<pinref part="J15" gate="G$1" pin="7"/>
<wire x1="233.68" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<wire x1="215.9" y1="116.84" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<junction x="215.9" y="119.38"/>
<pinref part="J15" gate="G$1" pin="8"/>
<wire x1="233.68" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<wire x1="215.9" y1="114.3" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="215.9" y="116.84"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1A"/>
<pinref part="J10" gate="G$1" pin="CLKRX-A"/>
<wire x1="185.42" y1="254" x2="187.96" y2="254" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="187.96" y1="254" x2="193.04" y2="254" width="0.1524" layer="91"/>
<junction x="187.96" y="254"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1B"/>
<pinref part="J10" gate="G$1" pin="CLKRX-B"/>
<wire x1="185.42" y1="243.84" x2="187.96" y2="243.84" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="187.96" y1="243.84" x2="193.04" y2="243.84" width="0.1524" layer="91"/>
<junction x="187.96" y="243.84"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1Y"/>
<pinref part="J10" gate="G$1" pin="TX-Y"/>
<wire x1="185.42" y1="238.76" x2="193.04" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1Z"/>
<pinref part="J10" gate="G$1" pin="TX-Z"/>
<wire x1="185.42" y1="233.68" x2="193.04" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2B"/>
<pinref part="J10" gate="G$1" pin="RX-B"/>
<wire x1="185.42" y1="226.06" x2="187.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="187.96" y1="226.06" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
<junction x="187.96" y="226.06"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2A"/>
<pinref part="J10" gate="G$1" pin="RX-A"/>
<wire x1="185.42" y1="215.9" x2="187.96" y2="215.9" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="187.96" y1="215.9" x2="193.04" y2="215.9" width="0.1524" layer="91"/>
<junction x="187.96" y="215.9"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2Z"/>
<pinref part="J10" gate="G$1" pin="CLKTX-Z"/>
<wire x1="185.42" y1="210.82" x2="193.04" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2Y"/>
<pinref part="J10" gate="G$1" pin="CLKRX-Y"/>
<wire x1="185.42" y1="205.74" x2="193.04" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ATK0-RXLED" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PF0"/>
<wire x1="88.9" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<label x="91.44" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="170.18" y1="193.04" x2="144.78" y2="193.04" width="0.1524" layer="91"/>
<label x="144.78" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-TXLED" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PF1/XCK0"/>
<wire x1="88.9" y1="55.88" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<label x="91.44" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="170.18" y1="182.88" x2="144.78" y2="182.88" width="0.1524" layer="91"/>
<label x="144.78" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-RX" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PF2/RXD0"/>
<wire x1="88.9" y1="53.34" x2="106.68" y2="53.34" width="0.1524" layer="91"/>
<label x="91.44" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="2R"/>
<wire x1="149.86" y1="220.98" x2="127" y2="220.98" width="0.1524" layer="91"/>
<label x="127" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-TX" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PF3/TXD0"/>
<wire x1="88.9" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<label x="91.44" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="1D"/>
<wire x1="149.86" y1="233.68" x2="127" y2="233.68" width="0.1524" layer="91"/>
<label x="127" y="233.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK-CLKOUT" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PF4"/>
<wire x1="88.9" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<label x="91.44" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="2D"/>
<wire x1="149.86" y1="205.74" x2="127" y2="205.74" width="0.1524" layer="91"/>
<label x="127" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-CLKIN" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1R"/>
<wire x1="149.86" y1="248.92" x2="127" y2="248.92" width="0.1524" layer="91"/>
<label x="127" y="248.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="PE4/SS"/>
<wire x1="88.9" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<label x="91.44" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="J10" gate="G$1" pin="LED-GRN-CATHODE"/>
<wire x1="180.34" y1="193.04" x2="193.04" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="LED-YLW-CATHODE"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="193.04" y1="182.88" x2="180.34" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PB2/DAC0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB2/DAC0"/>
<wire x1="88.9" y1="144.78" x2="106.68" y2="144.78" width="0.1524" layer="91"/>
<label x="91.44" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="3"/>
<wire x1="185.42" y1="127" x2="170.18" y2="127" width="0.1524" layer="91"/>
<label x="170.18" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB3/DAC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB3/DAC1"/>
<wire x1="88.9" y1="142.24" x2="106.68" y2="142.24" width="0.1524" layer="91"/>
<label x="91.44" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="185.42" y1="124.46" x2="170.18" y2="124.46" width="0.1524" layer="91"/>
<label x="170.18" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB1"/>
<wire x1="88.9" y1="147.32" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
<label x="91.44" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="185.42" y1="129.54" x2="170.18" y2="129.54" width="0.1524" layer="91"/>
<label x="170.18" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB0/AREF" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PB0/AREF"/>
<wire x1="88.9" y1="149.86" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<label x="91.44" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="185.42" y1="132.08" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
<label x="170.18" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC0" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC0/SDA"/>
<wire x1="88.9" y1="127" x2="106.68" y2="127" width="0.1524" layer="91"/>
<label x="91.44" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="5"/>
<wire x1="185.42" y1="121.92" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<label x="170.18" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC1/SCL/XCK0"/>
<wire x1="88.9" y1="124.46" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<label x="91.44" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="6"/>
<wire x1="185.42" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<label x="170.18" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC2/RXD0"/>
<wire x1="88.9" y1="121.92" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
<label x="91.44" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="7"/>
<wire x1="185.42" y1="116.84" x2="170.18" y2="116.84" width="0.1524" layer="91"/>
<label x="170.18" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC3/TXD0"/>
<wire x1="88.9" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="91.44" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="8"/>
<wire x1="185.42" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<label x="170.18" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC4/SS"/>
<wire x1="88.9" y1="116.84" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<label x="91.44" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="9"/>
<wire x1="185.42" y1="111.76" x2="170.18" y2="111.76" width="0.1524" layer="91"/>
<label x="170.18" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC5/XCK1/MOSI"/>
<wire x1="88.9" y1="114.3" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<label x="91.44" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="10"/>
<wire x1="185.42" y1="109.22" x2="170.18" y2="109.22" width="0.1524" layer="91"/>
<label x="170.18" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC6/RXD1/MISO"/>
<wire x1="88.9" y1="111.76" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<label x="91.44" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="11"/>
<wire x1="185.42" y1="106.68" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
<label x="170.18" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="PC7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PC7/TXD1/SCK"/>
<wire x1="88.9" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<label x="91.44" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="12"/>
<wire x1="185.42" y1="104.14" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<label x="170.18" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD4/SS"/>
<wire x1="88.9" y1="93.98" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<label x="91.44" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="13"/>
<wire x1="185.42" y1="101.6" x2="170.18" y2="101.6" width="0.1524" layer="91"/>
<label x="170.18" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD5/XCK/MOSI"/>
<wire x1="88.9" y1="91.44" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<label x="91.44" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="14"/>
<wire x1="185.42" y1="99.06" x2="170.18" y2="99.06" width="0.1524" layer="91"/>
<label x="170.18" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD6" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD6/RXD1/MISO/D-"/>
<wire x1="88.9" y1="88.9" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<label x="91.44" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="15"/>
<wire x1="185.42" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<label x="170.18" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="PD7" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="PD7/TXD1/SCK/D+"/>
<wire x1="88.9" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="91.44" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J9" gate="G$1" pin="16"/>
<wire x1="185.42" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<label x="170.18" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="1"/>
<pinref part="J7" gate="G$1" pin="1"/>
<wire x1="147.32" y1="132.08" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="134.62" y1="132.08" x2="121.92" y2="132.08" width="0.1524" layer="91"/>
<junction x="134.62" y="132.08"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="121.92" y1="129.54" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="2"/>
<wire x1="134.62" y1="129.54" x2="147.32" y2="129.54" width="0.1524" layer="91"/>
<junction x="134.62" y="129.54"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="3"/>
<pinref part="J7" gate="G$1" pin="3"/>
<wire x1="147.32" y1="127" x2="134.62" y2="127" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="134.62" y1="127" x2="121.92" y2="127" width="0.1524" layer="91"/>
<junction x="134.62" y="127"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="4"/>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="121.92" y1="124.46" x2="134.62" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="4"/>
<wire x1="134.62" y1="124.46" x2="147.32" y2="124.46" width="0.1524" layer="91"/>
<junction x="134.62" y="124.46"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="5"/>
<pinref part="J7" gate="G$1" pin="5"/>
<wire x1="147.32" y1="121.92" x2="134.62" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="5"/>
<wire x1="134.62" y1="121.92" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<junction x="134.62" y="121.92"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="6"/>
<pinref part="J7" gate="G$1" pin="6"/>
<wire x1="121.92" y1="119.38" x2="134.62" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="6"/>
<wire x1="134.62" y1="119.38" x2="147.32" y2="119.38" width="0.1524" layer="91"/>
<junction x="134.62" y="119.38"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="7"/>
<pinref part="J7" gate="G$1" pin="7"/>
<wire x1="147.32" y1="116.84" x2="134.62" y2="116.84" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="7"/>
<wire x1="134.62" y1="116.84" x2="121.92" y2="116.84" width="0.1524" layer="91"/>
<junction x="134.62" y="116.84"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="8"/>
<wire x1="121.92" y1="114.3" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="8"/>
<wire x1="132.08" y1="114.3" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="8"/>
<wire x1="134.62" y1="114.3" x2="147.32" y2="114.3" width="0.1524" layer="91"/>
<junction x="134.62" y="114.3"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="9"/>
<pinref part="J7" gate="G$1" pin="9"/>
<wire x1="147.32" y1="111.76" x2="134.62" y2="111.76" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="9"/>
<wire x1="134.62" y1="111.76" x2="121.92" y2="111.76" width="0.1524" layer="91"/>
<junction x="134.62" y="111.76"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="10"/>
<pinref part="J7" gate="G$1" pin="10"/>
<wire x1="121.92" y1="109.22" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="10"/>
<wire x1="134.62" y1="109.22" x2="147.32" y2="109.22" width="0.1524" layer="91"/>
<junction x="134.62" y="109.22"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="11"/>
<pinref part="J7" gate="G$1" pin="11"/>
<wire x1="147.32" y1="106.68" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="11"/>
<wire x1="134.62" y1="106.68" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<junction x="134.62" y="106.68"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="12"/>
<pinref part="J7" gate="G$1" pin="12"/>
<wire x1="121.92" y1="104.14" x2="134.62" y2="104.14" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="12"/>
<wire x1="134.62" y1="104.14" x2="147.32" y2="104.14" width="0.1524" layer="91"/>
<junction x="134.62" y="104.14"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="13"/>
<pinref part="J7" gate="G$1" pin="13"/>
<wire x1="147.32" y1="101.6" x2="134.62" y2="101.6" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="13"/>
<wire x1="134.62" y1="101.6" x2="121.92" y2="101.6" width="0.1524" layer="91"/>
<junction x="134.62" y="101.6"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="14"/>
<pinref part="J7" gate="G$1" pin="14"/>
<wire x1="121.92" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="14"/>
<wire x1="134.62" y1="99.06" x2="147.32" y2="99.06" width="0.1524" layer="91"/>
<junction x="134.62" y="99.06"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="15"/>
<pinref part="J7" gate="G$1" pin="15"/>
<wire x1="147.32" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="15"/>
<wire x1="134.62" y1="96.52" x2="121.92" y2="96.52" width="0.1524" layer="91"/>
<junction x="134.62" y="96.52"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="16"/>
<pinref part="J7" gate="G$1" pin="16"/>
<wire x1="121.92" y1="93.98" x2="134.62" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="16"/>
<wire x1="134.62" y1="93.98" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
<junction x="134.62" y="93.98"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
