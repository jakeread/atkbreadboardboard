/*
 * atkbbb.c
 *
 * Created: 9/21/2018 4:18:52 PM
 * Author : Jake
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "hardware.h"
#include "fastmath.h"
#include "pin.h"
#include "adc.h"

void clock_init(void){
	OSC.XOSCCTRL = OSC_XOSCSEL_XTAL_256CLK_gc | OSC_FRQRANGE_12TO16_gc; // select external source
	OSC.CTRL = OSC_XOSCEN_bm; // enable external source
	while(!(OSC.STATUS & OSC_XOSCRDY_bm)); // wait for external
	OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC0_bm | OSC_PLLFAC1_bm; // select external osc for pll, do pll = source * 3
	//OSC.PLLCTRL = OSC_PLLSRC_XOSC_gc | OSC_PLLFAC1_bm; // pll = source * 2 for 32MHz std clock
	OSC.CTRL |= OSC_PLLEN_bm; // enable PLL
	while (!(OSC.STATUS & OSC_PLLRDY_bm)); // wait for PLL to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_PLL_gc; // switch to PLL for main clock
}

void uarts_init(void){
	rb_init(&up0rbrx);
	rb_init(&up0rbtx);
	pin_init(&up0rxled, &PORTF, PIN0_bm, 0, 1);
	pin_init(&up0txled, &PORTF, PIN1_bm, 1, 1);
	uart_init(&up0, &USARTF0, &PORTF, PIN2_bm, PIN3_bm, &up0rbrx, &up0rbtx, &up0rxled, &up0txled);
	
	uart_start(&up0, SYSTEM_BAUDA, SYSTEM_BAUDB);
	
	ups[0] = &up0;
}

void atkps_init(void){
	atkport_init(&atkp0, 0, &up0);
}

// -------------------------------- SERVO PWM GEN CODE

void set_pwm_cca(uint16_t val){
	uint8_t vall = (uint8_t) val;
	uint8_t valh = (uint8_t) (val >> 8);
		
	TCC0.CCABUFL = vall;
	TCC0.CCABUFH = valh;
}

void servo_pwm_begin(void){
	PORTC.DIRSET = PIN0_bm;
	
	TCC0.CTRLA = TC_CLKSEL_DIV64_gc;
	TCC0.CTRLB = TC_WGMODE_SINGLESLOPE_gc | (1 << 4);
	
	uint16_t per = 15000;
	uint8_t perl = (uint8_t) per;
	uint8_t perh = (uint8_t) (per >> 8);
	
	TCC0.PERBUFL = perl;
	TCC0.PERBUFH = perh;

	// 750: 1ms
	// 1500: 2ms
	set_pwm_cca(750);
}

void set_servo(uint16_t val){
	// 0 -> 750 = signal 1000us -> 2000us duty on 20ms period
	if (val > 1500){
		val = 1500;
	}
	set_pwm_cca(750 + val);
}

// -------------------------------- ADC CODE

void init_adc(void){
	/*
	PORTB.DIRCLR = PIN0_bm;
	PORTB.DIRCLR = PIN1_bm;
	*/
	//ADCB.CALL = SP_ReadCalibrationByte(PROD_SIGNATURES_START + ADCBCAL0_offset);
	//ADCB.CALH = SP_ReadCalibrationByte(PROD_SIGNATURES_START + ADCBCAL1_offset);
		
	ADCB.CTRLB = ADC_RESOLUTION_12BIT_gc | ADC_CONMODE_bm;
	
	ADCB.REFCTRL = ADC_REFSEL_INTVCC_gc;
	
	ADCB.PRESCALER = ADC_PRESCALER_DIV64_gc;
		
	ADCB.CH0.CTRL = ADC_CH_INPUTMODE_DIFFWGAIN_gc | ADC_CH_GAIN_64X_gc;
	ADCB.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN1_gc | ADC_CH_MUXNEG_PIN0_gc;
	
	ADCB.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;
		
	// start it up 
	ADCB.CTRLA |= ADC_ENABLE_bm;
}

uint16_t get_adc(void){
	ADCB.CH0.CTRL |= ADC_CH_START_bm;
	while(!(ADCB.CH0.INTFLAGS & ADC_CH_CHIF_bm)){};
	uint8_t resl = ADCB.CH0.RESL;
	uint8_t resh = ADCB.CH0.RESH;
	uint16_t result = resh << 8 | resl;
	return result;//ADCB.CH0.RES;
}

uint16_t lookup[128] = {
	2047, 2147, 2247, 2347, 2446, 2544, 2641, 2736, 2830, 2922, 3011, 3099, 3184, 3266, 3345, 3421, 3494, 3563, 3629, 3691, 3749, 3802, 3852, 3897, 3938, 3974, 4005, 4032, 4054, 4071, 4084, 4091, 4094, 4091, 4084, 4071, 4054, 4032, 4005, 3974, 3938, 3897, 3852, 3802, 3749, 3691, 3629, 3563, 3494, 3421, 3345, 3266, 3184, 3099, 3011, 2922, 2830, 2736, 2641, 2544, 2446, 2347, 2247, 2147, 2047, 1947, 1847, 1747, 1648, 1550, 1453, 1358, 1264, 1172, 1083, 995, 910, 828, 749, 673, 600, 531, 465, 403, 345, 292, 242, 197, 156, 120, 89, 62, 40, 23, 10, 3, 0, 3, 10, 23, 40, 62, 89, 120, 156, 197, 242, 292, 345, 403, 465, 531, 600, 673, 749, 828, 910, 995, 1083, 1172, 1264, 1358, 1453, 1550, 1648, 1747, 1847, 1947
};

void init_dac(void){
	// DAC0 on PB2
	DACB.CTRLB = (DACB.CTRLB & ~DAC_CHSEL_gm) | DAC_CHSEL_DUAL_gc;
	DACB.CTRLC = (DACB.CTRLC & ~(DAC_REFSEL_gm | DAC_LEFTADJ_bm)) | DAC_REFSEL_AVCC_gc;
	DACB.CTRLA = (DACB.CTRLA & ~DAC_CH0EN_bm) | DAC_CH1EN_bm | DAC_ENABLE_bm;
}

int main(void)
{
	// start clock
	clock_init();
	
	// start networking hardware
	uarts_init();
	atkps_init();
	
	// allow interrupts (and set handlers below)
	sei();
	PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
	
	// lights
	pin_init(&stlclk, &PORTE, PIN2_bm, 2, 1);
	pin_init(&stlerr, &PORTE, PIN3_bm, 3, 1);
	pin_set(&stlerr);
	
	// servo begin
	servo_pwm_begin();
	
	// adc begin
	init_adc();
	
	// dac begin
	init_dac();
	
	uint16_t tck = 0;
	uint16_t angle = 0;
    while (1) 
    {
		//uint16_t res = get_adc();
		//DACB.CH1DATA = res;
		//DACB.CH1DATA = lookup[angle];
		//angle = (angle + 1) % 128;
		atkport_scan(&atkp0, 2);
		tck ++;
		if(!fastModulo(tck, 4096)){
			pin_toggle(&stlclk);
		}
    }
}


ISR(USARTF0_RXC_vect){
	uart_rxhandler(&up0);
}

ISR(USARTF0_DRE_vect){
	uart_txhandler(&up0);
}