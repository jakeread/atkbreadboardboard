/*
 * hardware.h
 *
 * Created: 6/18/2018 12:18:05 PM
 *  Author: Jake
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "pin.h"
#include "ringbuffer.h"
#include "uartport.h"
#include "atkport.h"
#include "atkhandler.h"

// A: 5 B: 0 - 2uS bit, 
// A: 11 B: 0 - 4uS bit, 250kBaud
#define SYSTEM_BAUDA 11
#define SYSTEM_BAUDB 0
#define SYSTEM_NUM_UPS 1

pin_t stlclk;
pin_t stlerr;

// UP0

ringbuffer_t up0rbrx;
ringbuffer_t up0rbtx;

uartport_t up0;

pin_t up0rxled;
pin_t up0txled;

atkport_t atkp0;

uartport_t *ups[SYSTEM_NUM_UPS];

// MAIN FCNS 

void set_servo(uint16_t val);

uint16_t get_adc(void);

#endif /* HARDWARE_H_ */